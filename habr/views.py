import re
from django.shortcuts import render
from django.http.response import HttpResponse
import requests
import html2text
habr_url = "https://habr.com"
h = html2text.HTML2Text()
h.ignore_links = True


def clean_text_from_spec_symbols(word):
    word = re.sub('[^\w\d]+', '', word)
    return word.strip()


def clean_text_from_html(word):
    clean_text = h.handle(word)
    return clean_text


def clean_from_broken_html(word):
    index = word.find(">")
    if index > -1:
        word = word[index + 1:]
    index = word.find("<")
    if index > -1:
        word = word[:index]
    return word


def find_all_words(list_of_words):
    all_words_dict = {}
    for word in list_of_words:
        if len(word) == 6:
            all_words_dict[word] = '{}&trade;'.format(word)
    return all_words_dict


def fetch_some_resource(request):
    """
    For some reason I can't get access to resource habr files (*.ico, *.png, etc...)
    """
    resource_url = habr_url + request.get_full_path()
    r = requests.get(resource_url)
    return HttpResponse(r.content)


def main(request):
    http_url = request.get_full_path().lstrip('/')

    if 'http' not in http_url:
        return fetch_some_resource(request)
    else:
        proxy_protocol = 'https' if request.is_secure() else 'http'
        proxy = proxy_protocol + '://' + request.get_host()

        r = requests.get(http_url)

        utf8_content = r.content.decode()
        clean_text = clean_text_from_html(utf8_content)

        all_words_list = clean_text.replace('\n', ' ')
        all_words_list = [clean_text_from_spec_symbols(s) for s in all_words_list.split(' ')]
        all_words_list = [s for s in all_words_list if len(s)]

        all_words_dict = find_all_words(all_words_list)

        response_array = utf8_content.replace('>', '> ')
        response_array = response_array.replace('<', ' <')
        response_array = response_array.replace('<a href="http', '<a href="{}/http'.format(proxy))
        response_array = response_array.split(' ')
        response_array = [s for s in response_array if len(s.strip())]

        for idx, word_with_tags in enumerate(response_array):
            for key in all_words_dict:
                if key in word_with_tags:
                    cleaned_word = clean_text_from_html(word_with_tags)
                    cleaned_word = clean_from_broken_html(cleaned_word)
                    cleaned_word = clean_text_from_spec_symbols(cleaned_word)

                    if key == cleaned_word:
                        response_array[idx] = response_array[idx].replace(key, all_words_dict[key])
                        break
        return render(request, 'habr/main.html', {"content": " ".join(response_array)})
